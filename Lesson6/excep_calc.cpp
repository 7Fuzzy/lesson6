#include <iostream>
using namespace std;

int Add(int a, int b) {
	if (a == 8200 || b == 8200 || a + b == 8200)
	{
		throw(std::string("This user is not authorized to access 8200, please enter different numbers, or try to getclearance in 1 year."));
	}
  return a + b;
}

int Multiply(int a, int b) {
  int sum = 0;
  for(int i = 0; i < b; i++) {
    sum = Add(sum, a);
  };
  return sum;
}

int Pow(int a, int b) {
  int exponent = 1;
  for(int i = 0; i < b; i++) {
    exponent = Multiply(exponent, a);
  };
  return exponent;
}

int main()
{
	try
	{
		int power = Pow(4, 5);
		cout << power << endl;
		int sum = Add(8000, 200);
		cout << sum << endl;
	}
	catch (const string& e)
	{
		cout << "ERROR: " << e.c_str() << endl;
	}
	getchar();
	return 0;
}