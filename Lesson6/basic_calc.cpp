#include <iostream>
using namespace std;

int Add(int a, int b, bool* err) {
  int sum  = a + b;
  *err = sum == 8200 || a == 8200 || b == 8200;
  return sum;
}

int Multiply(int a, int b, bool* err) {
  int sum = 0;
  for(int i = 0; i < b; i++) {
    sum = Add(sum, a, err);
  };
  if (sum == 8200)
  {
	  *err = true;
  }
  return sum;
}

int Pow(int a, int b, bool* err) {
  int exponent = 1;
  for(int i = 0; i < b; i++) {
    exponent = Multiply(exponent, a, err);
  };
  if (exponent == 8200)
  {
	  *err = true;
  }
  return exponent;
}

/*
	Comment the other main to use this one
*/
//int main()
//{
//	bool* isError = new bool;
//	*isError = false;
//
//	int power = Pow(4, 5, isError);
//	if (!*isError)
//	{
//		cout << power << endl;
//	}
//	else
//	{
//		cout << "This user is not authorized to access 8200, please enter different numbers, or try to getclearance in 1 year." << endl;
//		*isError = false;
//	}
//	int sum = Add(8000, 200, isError);
//	if (!*isError)
//	{
//		cout << sum << endl;
//	}
//	else
//	{
//		cout << "This user is not authorized to access 8200, please enter different numbers, or try to getclearance in 1 year." << endl;
//		*isError = false;
//	}
//	getchar();
//	return 0;
//}