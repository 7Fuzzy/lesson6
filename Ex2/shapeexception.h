#pragma once
#include <exception>
using namespace std;

class shapeException : public exception
{
private: 
	string _msg;
public: 
	shapeException(string msg)
	{
		this->_msg = msg;
	}
	virtual const char* what() const
	{
		return this->_msg.c_str();
	}
};