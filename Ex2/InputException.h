#pragma once
#include <exception>
#include <iostream>
using namespace std;

class InputException : public exception
{
public:
	virtual const char* what() const
	{
		if (cin.fail())
		{
			cin.clear();
			std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
			return "Input is invalid\n";
		}
	}
};
