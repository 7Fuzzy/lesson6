#include "Hexagon.h"
#include <iostream>
#include "shapeexception.h"
#include "InputException.h"
#include "MathUtlis.h"

Hexagon::Hexagon(std::string name, std::string col, double len)
	: Shape(name, col)
{
	setLength(len);
}

void Hexagon::setLength(double len)
{
	if (len < 0)
	{
		throw shapeException("Length is lower than 0\n");
	}
	else if (cin.fail())
	{
		throw InputException();
	}
	this->_len = len;
}

double Hexagon::getLength()
{
	return this->_len;
}

void Hexagon::draw()
{
	cout << "Name: " << getName() << " Color: " << getColor() << " Length: " << getLength() << "Perimeter: " << MathUtlis::CalHexagonArea(this->_len) << endl;
}

