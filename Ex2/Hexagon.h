#pragma once
#include "shape.h"
class Hexagon : public Shape
{
private:
	double _len;
public:
	Hexagon(std::string name, std::string col, double length);
	void setLength(double len);
	double getLength();
	virtual void draw();
};

