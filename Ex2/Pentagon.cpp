#include "Pentagon.h"
#include "shapeexception.h"
#include "InputException.h"
#include "MathUtlis.h"
using namespace std;



Pentagon::Pentagon(string name, std::string col, double len) 
	: Shape(name, col)
{
	this->setLength(len);
}

void Pentagon::setLength(double len)
{
	if (len < 0)
	{
		throw shapeException("Length is lower than 0\n");
	}
	else if (cin.fail())
	{
		throw InputException();
	}
	this->_len = len;
}

double Pentagon::getLength()
{
	return this->_len;
}

void Pentagon::draw()
{
	cout << "Name: " << getName() << " Color: " << getColor() << " Length: " << getLength() << "Perimeter: " << MathUtlis::CalPentagonArea(this->_len) << endl;
}