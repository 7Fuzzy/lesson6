#pragma once
#include "shape.h"
class Pentagon : public Shape
{
private:
	double _len;
public:
	Pentagon(std::string name, std::string col, double length);
	void setLength(double len);
	double getLength();
	virtual void draw();
};

