#include "MathUtlis.h"
#include <math.h>

double MathUtlis::CalPentagonArea(double len)
{
	return 0.25 * sqrt(5 * (5 + 2 * sqrt(5))) * len * len;
}
double MathUtlis::CalHexagonArea(double len)
{
	return 3.0 / 2 * sqrt(3) * len * len;
}
